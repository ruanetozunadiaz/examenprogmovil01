package com.example.examenprogmovil01;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText etUsuario, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etUsuario = findViewById(R.id.etUsuario);
        etPassword = findViewById(R.id.etPassword);

        Button btnLogin = findViewById(R.id.btnLogin);
        Button btnSalir = findViewById(R.id.btnSalir);

        btnLogin.setOnClickListener(v -> login());
        btnSalir.setOnClickListener(v -> finish()); // Cierra la aplicación
    }

    private void login() {
        String usuario = etUsuario.getText().toString();
        String password = etPassword.getText().toString();

        if (usuario.equals("admin") && password.equals("1234")) {
            Intent intent = new Intent(MainActivity.this, CuentaBancoActivity.class);
            intent.putExtra("USUARIO", usuario);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Credenciales incorrectas", Toast.LENGTH_SHORT).show();
        }
    }
}
