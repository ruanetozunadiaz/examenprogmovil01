package com.example.examenprogmovil01;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class CuentaBancoActivity extends AppCompatActivity {

    private EditText etNumCuenta, etNombre, etBanco, etSaldo, etCantidad, etNuevoSaldo;
    private TextView tvMovimientos, tvUsuario;
    private Spinner spinnerMovimientos;
    private CuentaBanco cuentaBanco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta_banco);

        etNumCuenta = findViewById(R.id.etNumCuenta);
        etNombre = findViewById(R.id.etNombre);
        etBanco = findViewById(R.id.etBanco);
        etSaldo = findViewById(R.id.etSaldo);
        etCantidad = findViewById(R.id.etCantidad);
        etNuevoSaldo = findViewById(R.id.etNuevoSaldo);
        tvMovimientos = findViewById(R.id.tvMovimientos);
        tvUsuario = findViewById(R.id.tvUsuario);
        spinnerMovimientos = findViewById(R.id.spinnerMovimientos);

        // Configuración del Spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.movimientos_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMovimientos.setAdapter(adapter);

        String usuario = getIntent().getStringExtra("USUARIO");
        tvUsuario.setText("Usuario: " + usuario);

        // Generar un número de cuenta aleatorio
        etNumCuenta.setText(generarNumeroCuenta());

        // Inicializar cuentaBanco con datos iniciales
        inicializarCuentaBanco();

        Button btnAplicar = findViewById(R.id.btnAplicar);
        Button btnRegresar = findViewById(R.id.btnRegresar);
        Button btnLimpiar = findViewById(R.id.btnLimpiar);

        btnAplicar.setOnClickListener(v -> aplicarMovimiento());
        btnRegresar.setOnClickListener(v -> finish()); // Regresa a la actividad anterior
        btnLimpiar.setOnClickListener(v -> limpiarCampos());
    }

    private void inicializarCuentaBanco() {
        try {
            String numCuenta = etNumCuenta.getText().toString();
            String nombre = etNombre.getText().toString();
            String banco = etBanco.getText().toString();
            double saldo = Double.parseDouble(etSaldo.getText().toString());

            cuentaBanco = new CuentaBanco(numCuenta, nombre, banco, saldo);
            Toast.makeText(this, "Cuenta registrada exitosamente.", Toast.LENGTH_SHORT).show();
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Por favor ingrese un saldo inicial válido.", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Ocurrió un error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void aplicarMovimiento() {
        try {
            String movimiento = spinnerMovimientos.getSelectedItem().toString();
            double cantidad = Double.parseDouble(etCantidad.getText().toString());

            if (cuentaBanco == null) {
                inicializarCuentaBanco();
            }

            switch (movimiento) {
                case "Consultar":
                    consultarSaldo();
                    break;
                case "Depositar":
                    cuentaBanco.hacerDeposito(cantidad);
                    tvMovimientos.setText("Depósito realizado. Nuevo saldo: " + cuentaBanco.getSaldo());
                    etNuevoSaldo.setText(String.valueOf(cuentaBanco.getSaldo()));
                    break;
                case "Retirar":
                    boolean exito = cuentaBanco.hacerRetiro(cantidad);
                    if (exito) {
                        tvMovimientos.setText("Retiro realizado. Nuevo saldo: " + cuentaBanco.getSaldo());
                        etNuevoSaldo.setText(String.valueOf(cuentaBanco.getSaldo()));
                    } else {
                        tvMovimientos.setText("Saldo insuficiente.");
                    }
                    break;
                default:
                    Toast.makeText(this, "Movimiento no válido", Toast.LENGTH_SHORT).show();
                    break;
            }
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Por favor ingrese una cantidad válida.", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Ocurrió un error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void consultarSaldo() {
        if (cuentaBanco != null) {
            tvMovimientos.setText("Saldo actual: " + cuentaBanco.getSaldo());
            etNuevoSaldo.setText(String.valueOf(cuentaBanco.getSaldo()));
        }
    }

    private String generarNumeroCuenta() {
        Random random = new Random();
        int numeroCuenta = random.nextInt(1000000);
        return String.format("%06d", numeroCuenta);
    }

    private void limpiarCampos() {
        etNumCuenta.setText(generarNumeroCuenta());
        etNombre.setText("");
        etBanco.setText("");
        etSaldo.setText("");
        etCantidad.setText("");
        etNuevoSaldo.setText("");
        tvMovimientos.setText("");
    }
}
