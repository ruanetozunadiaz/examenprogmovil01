package com.example.examenprogmovil01;

public class CuentaBanco {
    private String numCuenta;
    private String nombre;
    private String banco;
    private double saldo;

    public CuentaBanco(String numCuenta, String nombre, String banco, double saldo) {
        this.numCuenta = numCuenta;
        this.nombre = nombre;
        this.banco = banco;
        this.saldo = saldo;
    }

    public String getNumCuenta() {
        return numCuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public String getBanco() {
        return banco;
    }

    public double getSaldo() {
        return saldo;
    }

    public void hacerDeposito(double cantidad) {
        if (cantidad > 0) {
            saldo += cantidad;
        }
    }

    public boolean hacerRetiro(double cantidad) {
        if (cantidad > 0 && cantidad <= saldo) {
            saldo -= cantidad;
            return true;
        }
        return false;
    }
}
